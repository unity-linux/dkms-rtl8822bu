# Disable the building of the debug package(s).
%define debug_package %{nil}
# Define the dkms package name here.
%define dkms_name rtl8822bu
%define rmod_name 88x2bu

# If kversion isn't defined on the rpmbuild line, define it here.
%{!?kversion: %define kversion %(uname -r)}

Name:    dkms-%{dkms_name}
Version: 20190112
Release: 0.1%{?dist}
Group:   System Environment/Kernel
Summary: %{dkms_name} kernel dkms_name(s)
License:    GPL
URL:        https://github.com/MeissnerEffect/rtl8822bu
Source0:    https://github.com/MeissnerEffect/rtl8822bu/archive/master.zip
Source1:    %{dkms_name}-dkms.conf

BuildRequires: perl
BuildRequires: bc
BuildRequires: bsdtar
BuildRequires: redhat-rpm-config
BuildRequires: dkms
BuildRequires: kernel-devel
Requires: kernel-devel
Requires(post): dkms
Requires(preun): dkms
ExclusiveArch: x86_64 i686 armv7hl

%description
This package provides the %{dkms_name} kernel dkms_name(s).
It is built to depend upon the specific ABI provided by a range of releases
of the same variant of the Linux kernel and not on any one specific build.

%prep
#%setup -q -n %{dkms_name}-%{version}
%setup -q -c -T -n %{dkms_name}-%{version}
bsdtar -xf %{SOURCE0} -s'|[^/]*/||'
cp %{SOURCE1} ./dkms.conf

%build
#

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/src/%{dkms_name}-%{version}/
cp -r * %{buildroot}/usr/src/%{dkms_name}-%{version}

%clean
%{__rm} -rf %{buildroot}

%post
occurrences=/usr/sbin/dkms status | grep "%{dkms_name}" | grep "%{version}" | wc -l
if [ ! occurrences > 0 ];
then
    /usr/sbin/dkms add -m %{dkms_name} -v %{version}
fi
/usr/sbin/dkms build -m %{dkms_name} -v %{version}
/usr/sbin/dkms install -m %{dkms_name} -v %{version}
exit 0

%preun
/usr/sbin/dkms remove -m %{dkms_name} -v %{version} --all
exit 0

%files
%defattr(0644,root,root)
%attr(0755,root,root) /usr/src/%{dkms_name}-%{version}/

%changelog
* Sun Jan 20 2019 JMiahMan <jmiahman@unity-linux.org> - 20181226-0.1
- Initial build
